# updateDVB_binaries_i386

https://bitbucket.org/bluzee/updatedvb_binaries_i386/downloads/

Deb packages for v4l-updatelee kernel and the updateDVB scanning program.

These should install into 32bit Mint 18 distros, Ubuntu 16.04 distro or any
other Xenial or newer based distro.

**Warning**

If you are using any DKMS drivers such as nVidia proprietary drivers these may
need to be re-installed after installing the new kernel. Ensure there are drivers
available for the kernel version before proceeding. It is highly recommended to switch 
to the open source *nouveau* driver before rebooting.  If the nvidia driver fails
to load reboot and select your old kernel to boot from the GRUB menu. The v4l-updatedvb 
kernel packages can be safely removed. You will not be able to use updateDVB without
the patched kernel drivers installed however. 

Download the linux-image linux-headers and linux-libc-dev packages
into a seperate directory and install with...

sudo dpkg -i *.deb

Reboot. If the v4l-updatedvb kernel version is newer than your original kernel
it should boot by default. If not you'll have to select it from your grub menu.
You can check which kernel version booted with....

uname -a

Download the updateDVB deb file.  Use *gdebi* to install it as it will require
a number of QT5 packages to be installed as dependencies.

Some DVB devices may also require firmware files to be installed to /lib/firmware
before they will work if you have not done so already.  

You should now be able to run updateDVB and spectrum scan with your supported DVB
device. See https://gitlab.com/updatelee/updateDVB
